/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataSystem;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author alextc6
 */
public class PalabrasReservadasC {
    
    // usados en el analizarLexico
    private String sentenciaCiclo = "while";
    private String incioCondicion = "(";
    private String finCondicion = ")";
    private String inicioCuerpo = "{";
    private String finCuerpo = "}";
    private String finInstruccion = ";";
    
    // utilizados para crear la lista de tokens
    private List<String> separadoresAlmacenables = Arrays.asList(";","(",")","{","}","<",">","=","!","|","&","/","*");
    private List<String> separadoresVacios = Arrays.asList(" ","\t","\n");
    
    // utilizados para crear la lista de tipos de tokens
    private List<String> comparadoresInicio = Arrays.asList("<",">");
    private String comparadorFIn = "=";
    private String comparadorDistinto = "!";
    private List<String> tiposDeDatos = Arrays.asList("int", "char");
    private List<String> operadoresMatematicos1 = Arrays.asList("+","-");
    private List<String> operadoresMatematicos2 = Arrays.asList("*","/");
    
    
    // salida de datos
    public static final String TIPO_DE_DATO = "t";
    public static final String OPERADOR_MATEMATICO_1 = "r";
    public static final String OPERADOR_MATEMATICO_2 = "q";
    public static final String OPERADOR_LOGICO = "l";
    public static final String COMPARADOR = "c";
    public static final String OPERADOR = "o";
    public static final String SENTENCIA_CICLO = "w";
    public static final String PARENTESIS_ABRIR = "(";
    public static final String PARENTESIS_CERRAR = ")";
    public static final String LLAVE_ABRIR = "{";
    public static final String LLAVE_CERRAR = "}";
    public static final String INSTRUCCION = "i";
    
    public static final String OP_MAT_PRMIARIA = "p";
    public static final String OP_MAT_SECUNDARIA = "s";
    
    public static final String INCREMENTO = "a";
    
    public static final String LOGICO_OR = "|";
    public static final String LOGICO_AND = "&";
    public static final String COMPARADOR_INICIO = "y";
    public static final String COMPARADOR_FIN = "f";
    public static final String COMPARADOR_DISTINTO = "d";
    public static final String FIN_INSTRUCCION = ";";
    
    public static final String FIN_DE_CADENA = "$";
    
    
    public PalabrasReservadasC(){
        
    }
    
    public boolean esNombreVariable(String cadena){
        Pattern p = Pattern.compile("^[a-z]");
        Matcher m = p.matcher(cadena);
        
        if (m.find())
            return true;
        
        return false;
    }
    
    // analiza si son i++, i--
    public boolean esIncremento(String cadena){

        /* #################### FUNCIONAN #################### */
        Pattern ini = Pattern.compile("^[a-z]");
        Matcher i = ini.matcher(cadena);
        Pattern incremental = Pattern.compile(".[+]{2}$");
        Matcher m = incremental.matcher(cadena);
        Pattern decremental = Pattern.compile(".[-]{2}$");
        Matcher n = decremental.matcher(cadena);
        
        if(i.find()){
            if(m.find()){
                return true;
            }
            if(n.find()){
                return true;
            }
        }
        
        return false;
    }
    
    public boolean esOperadorMatematico1(String token){
        // vemos si el token es una palabra permitida por el lenguaje
        if(this.operadoresMatematicos1.contains(token))
            return true;
        return false;
    }
    
    public boolean esOperadorMatematico2(String token){
        // vemos si el token es una palabra permitida por el lenguaje
        if(this.operadoresMatematicos2.contains(token))
            return true;
        return false;
    }
    
    public boolean esComparadorInicio(String token){
        if(this.comparadoresInicio.contains(token))
            return true;
        return false;
    }
    
    public boolean esComparadorFin(String token){
        if(token.equals(this.comparadorFIn))
            return true;
        return false;
    }
    
    public boolean esComparadorDistinto(String token){
        if(token.equals(this.comparadorDistinto))
            return true;
        return false;
    }
    
    public boolean esTipoDeDato(String token){
        if(this.tiposDeDatos.contains(token))
            return true;
        return false;
    }
    
    public boolean esSentenciaCiclo(String token){
        if(this.sentenciaCiclo.equals(token))
            return true;
        return false;
    }
    
    public boolean esSeparadorAlmacenable(String token){
        if(this.separadoresAlmacenables.contains(token))
            return true;
        return false;
    }
    
    public boolean esSeparadorVacio(String token){
        if(this.separadoresVacios.contains(token))
            return true;
        return false;
    }
}