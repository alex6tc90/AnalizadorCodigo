/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebas;

import controller.AnalizadorSintactico;
import controller.ControladorSistema;
import controller.ControladorTablaLR;
import controller.ControladorAnalizadorSintactico;
import controller.TokenizadorC;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author alextc6
 */
public class MainFinal {
    public static void main(String[] args) throws IOException {
             
        // ******************************** AnalisisCompleto(con grafica) **********************************

        String rutaDatosAnalizadorLexico = "/home/alextc6/NetBeansProjects/AnalizadorCompleto/src/datosTxt/configAnalizadorLexico_while.txt";
        String rutaDatosAnalizadorSintactico = "/home/alextc6/NetBeansProjects/AnalizadorCompleto/src/datosTxt/configAnalizadorSintactico_while.txt";
        
        TokenizadorC tokenizador = new TokenizadorC();
        
        ControladorSistema controladorSistema = new ControladorSistema(rutaDatosAnalizadorLexico, rutaDatosAnalizadorSintactico, tokenizador);
        // datos para el analizadorLR
        final pruebas.PantallaAnalizadorLR pantalla = new pruebas.PantallaAnalizadorLR(controladorSistema);
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                pantalla.setVisible(true);
            }
        });
    }

}
