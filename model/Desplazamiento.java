/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alextc6
 */
public class Desplazamiento {
    
    private int nuevoEstado;
    
    public Desplazamiento(int nuevoEstado){
        this.nuevoEstado = nuevoEstado;
    }

    public int getNuevoEstado() {
        return nuevoEstado;
    }

    public void setNuevoEstado(int nuevoEstado) {
        this.nuevoEstado = nuevoEstado;
    }
        
    @Override
    public String toString() {
        return "D" + nuevoEstado;
    }
    
}
