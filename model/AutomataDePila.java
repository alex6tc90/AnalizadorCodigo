package model;

import dataSystem.PalabrasReservadasC;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * El automata de pila esta representado por una lista de estados y una lista de 
 * que contiene las transiciones de cada estado.
 * 
 * @author emanuel
 */
public class AutomataDePila {

    private String lenguaje;
    private List<Estado> estados;
    private Map< String, List<Transicion>> transiciones;

    public AutomataDePila(String lenguaje) {
        this.lenguaje = lenguaje;
        this.estados = new ArrayList<>();
        this.transiciones = new HashMap<>();
    }

    /**
     * Este metodo se ocupa de buscar transiciones de un estado.
     * @param estado nombre del estado
     * @return una lista de los estados con que transiciona el estado actual
     */
    public List<Transicion> buscarTransisiones(String estado) {
        return transiciones.get(estado);
    }
    
    /**
     * Este metodo se ocupa de buscar un estado segun su nombre
     * @param nombre del estado a buscar
     * @return el estado si es que existe en la lista de estados
     */
    public Estado buscarEstado(String nombre){
        for (Estado estado : estados) {
            if(estado.getNombre().equals(nombre)){
             return estado;   
            }
        }
        return null;
    }
    
    public void agregarEstado(Estado estado){
        this.estados.add(estado);
    }    
    
    public Estado getEstado(int i){
        return estados.get(i);
    }
    
    /**
     * Este metodo permite agregar las transiciones de un estado
     * @param estado
     * @param trans transiciones 
     */
    public void agregarTransiciones(String estado, List<Transicion> trans) {
        this.transiciones.put(estado, trans);
    }
    
    /* Devuelve los caracteres del alfabeto del automata
    */
    public List<String> getAlfabeto(){
        List<String> alfabetoAutomata = new ArrayList<>();
        List<Transicion> transicionesDeLlave;
        String posibleComponenteAlfabeto;
        // recorremos las claves
        for (String llave : transiciones.keySet()) {
            // obtenemos las transiciones de cada llave
            transicionesDeLlave = this.transiciones.get(llave);
            for (Transicion transicionDeLlave : transicionesDeLlave) {
                posibleComponenteAlfabeto = transicionDeLlave.getEntrada();
                if((!posibleComponenteAlfabeto.equals(PalabrasReservadasC.FIN_DE_CADENA))&&(!alfabetoAutomata.contains(posibleComponenteAlfabeto))&&(posibleComponenteAlfabeto.compareTo("-") != 0)){
                    alfabetoAutomata.add(posibleComponenteAlfabeto);
                }
            }
        }
        return alfabetoAutomata;
    }

    public String getLenguaje() {
        return lenguaje;
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < estados.size(); i++) {
            s+=estados.get(i)+"\t"+transiciones.values().toArray()[i]+"\n";
        }

        return s;
    }


}
