/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author emanuel
 */
public class Estado {

    private String nombre;
    private boolean aceptable;


    public Estado(String nombre, boolean aceptable) {
        this.nombre = nombre;
        this.aceptable = aceptable;
    }

    public Estado() {
    }

    @Override
    public String toString() {
        return "<" + nombre +", "+ ((aceptable == true)? "1":"0" )+">";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String o) {
        this.nombre = o;

    }

    public boolean isAceptable() {
        return aceptable;
    }

    public void setAcceptable(boolean aceptable) {
        this.aceptable = aceptable;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + Objects.hashCode(this.nombre);
        hash = 29 * hash + (this.aceptable ? 1 : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estado other = (Estado) obj;
        if (this.aceptable != other.aceptable) {
            return false;
        }
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }
    
}
