/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alextc6
 */
public class Aceptador {
    
    private boolean aceptador;
    
    public Aceptador(){
        this.aceptador = true;
    }

    public boolean isAceptador() {
        return aceptador;
    }

    public void setAceptador(boolean aceptador) {
        this.aceptador = aceptador;
    }

    @Override
    public String toString() {
        if(this.isAceptador())
            return "A";
        return "NA";
    }

}
