/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alextc6
 */
public class DatosSeguimientoPila {
    
    private String caracter;
    private String estadoDestino;
    private boolean aceptador;
    private String contenidoPila;
    
    public DatosSeguimientoPila(){
    }
    
    public DatosSeguimientoPila(String caracter, String estadoDestino, boolean aceptador, String contenidoPila){
        this.caracter = caracter;
        this.estadoDestino = estadoDestino;
        this.aceptador = aceptador;
        this.contenidoPila = contenidoPila;
    }

    public String getCaracter() {
        return caracter;
    }

    public String getEstadoDestino() {
        return estadoDestino;
    }

    public String getContenidoPila() {
        return contenidoPila;
    }

    public void setCaracter(String caracter) {
        this.caracter = caracter;
    }

    public void setEstadoDestino(String estadoDestino) {
        this.estadoDestino = estadoDestino;
    }

    public void setContenidoPila(String contenidoPila) {
        this.contenidoPila = contenidoPila;
    }

    public boolean isAceptador() {
        return aceptador;
    }

    public void setAceptador(boolean aceptador) {
        this.aceptador = aceptador;
    }
        
}
