/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author alextc6
 */
public class Reduccion {
    
    private String izqRegla;
    private String derRegla;
    
    public Reduccion(String izqRegla, String derRegla){
        this.izqRegla = izqRegla;
        this.derRegla = derRegla;
    }

    public String getIzqRegla() {
        return izqRegla;
    }

    public void setIzqRegla(String izqRegla) {
        this.izqRegla = izqRegla;
    }

    public String getDerRegla() {
        return derRegla;
    }

    public void setDerRegla(String derRegla) {
        this.derRegla = derRegla;
    }

    @Override
    public String toString() {
        return ""+ izqRegla + "-->" + derRegla;
    }
}
