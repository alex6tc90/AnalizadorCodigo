/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author emanuel
 */
public class Transicion {
//    cada tupla de 4 elem representa 1 transicion ( entrada, estado sucesor, pop pila, push pila).
    
    private String entrada;
    private String estadoSucesor;
    private String desapila;
    private String apila;

    public Transicion(String entrada, String estadoSucesor, String desapila, String apila) {
        this.entrada = entrada;
        this.estadoSucesor = estadoSucesor;
        this.desapila = desapila;
        this.apila = apila;
    }

    public String getEntrada() {
        return entrada;
    }

    public void setEntrada(String entrada) {
        this.entrada = entrada;
    }

    public String getEstadoSucesor() {
        return estadoSucesor;
    }

    public void setEstadoSucesor(String estadoSucesor) {
        this.estadoSucesor = estadoSucesor;
    }

    public String getDesapila() {
        return desapila;
    }

    public void setDesapila(String desapila) {
        this.desapila = desapila;
    }

    public String getApila() {
        return apila;
    }

    public void setApila(String apila) {
        this.apila = apila;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.entrada);
        hash = 67 * hash + Objects.hashCode(this.estadoSucesor);
        hash = 67 * hash + Objects.hashCode(this.desapila);
        hash = 67 * hash + Objects.hashCode(this.apila);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Transicion other = (Transicion) obj;
        if (!Objects.equals(this.entrada, other.entrada)) {
            return false;
        }
        if (!Objects.equals(this.desapila, other.desapila)) {
            return false;
        }
        if (!Objects.equals(this.apila, other.apila)) {
            return false;
        }
        if (!Objects.equals(this.estadoSucesor, other.estadoSucesor)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "   { " + entrada + ", " + estadoSucesor + ", " + desapila + ", " + apila + " }   ";
    }

    
    
}
