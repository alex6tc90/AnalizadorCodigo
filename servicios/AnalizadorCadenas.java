/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alextc6
 */
public class AnalizadorCadenas {
    
    public AnalizadorCadenas(){
    }
    
    /* Determina si los caracteres de "cadena" corresponden a la lista
    */
    public boolean caracteresPertenecen(List<String> cadenas, String cadena){
        List<String> cadenaSeparada = separarCadena(cadena);
        // vemos si los caracteres de la cadena separados pertenecen a la lista
        for (String elementoLista : cadenaSeparada) {
            if(!cadenas.contains(elementoLista)){
                return false;
            }
        }
        return true;
    }
    
    /* Separa por caracter una cadena en una lista de cadenas, despreciando los espacios, tabulaciones y comas
     */
    public static List<String> separarCadena(String cadena){
        List<String> cadenaSeparada = new ArrayList<>();
        char character;
        for (int i = 0; i < cadena.length(); i++){
            character = cadena.charAt(i);
            // vemos si son espacios o tabuladores para descartarlos
            if((character != '\t')&&(character != ' ')&&(character != ',')){
                cadenaSeparada.add(""+String.valueOf(character));
            }
        }
        return cadenaSeparada;
    }
    
    /* Separa por caracter una cadena en una lista de cadenas, despreciando los espacios
     */
    public static List<String> separarCadenaEspacios(String cadena){
        List<String> cadenaSeparada = new ArrayList<>();
        char character;
        for (int i = 0; i < cadena.length(); i++){
            character = cadena.charAt(i);
            // vemos si son espacios o tabuladores para descartarlos
            if(character != ' '){
                cadenaSeparada.add(""+String.valueOf(character));
            }
        }
        return cadenaSeparada;
    }
    
    /* Separa por caracter una cadena en una lista de cadenas, despreciando los espacios, tabulaciones y enter
     */
    public static List<String> separarCadenaSinEspacios(String cadena){
        List<String> cadenaSeparada = new ArrayList<>();
        char character;
        for (int i = 0; i < cadena.length(); i++){
            character = cadena.charAt(i);
            // vemos si son espacios o tabuladores para descartarlos
            if((character != ' ')&&(character != '\t')&&(character != '\n')){
                cadenaSeparada.add(""+String.valueOf(character));
            }
        }
        return cadenaSeparada;
    }
    
    /* Separa por caracter una cadena en una lista de cadenas
     */
    public static List<String> separarCadenaCaracter(String cadena){
        List<String> cadenaSeparada = new ArrayList<>();
        char character;
        for (int i = 0; i < cadena.length(); i++){
            character = cadena.charAt(i);
            cadenaSeparada.add(""+String.valueOf(character));
        }
        return cadenaSeparada;
    }
    
}
