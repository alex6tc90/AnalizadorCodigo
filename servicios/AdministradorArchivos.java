/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

/**
 *
 * @author alextc6
 */
public class AdministradorArchivos {
    
    private static AdministradorArchivos ADMIN = null;
    
    public AdministradorArchivos(){
    }
    
    public static AdministradorArchivos getAdminArchivos(){
        if(ADMIN == null){
            ADMIN = new AdministradorArchivos();
        }
        return ADMIN;
    }
    
    // devuelve un buffer con los datos del archivo correspondiente al path recibido
    public BufferedReader getBufferDatos(String pathArchivo) throws FileNotFoundException{
        File archivo = new File (pathArchivo);
        FileReader fr = new FileReader (archivo);
        BufferedReader buffer = new BufferedReader(fr);
        return buffer;
    }
    
}
