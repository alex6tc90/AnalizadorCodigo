/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dataSystem.PalabrasReservadasC;
import java.util.ArrayList;
import java.util.List;
import services.AnalizadorCadenas;

/**
 *
 * @author alextc6
 */
public class TokenizadorC {
    
    private PalabrasReservadasC palabrasReservadasDelLenguaje;
    
    public TokenizadorC(){
        this.palabrasReservadasDelLenguaje = new PalabrasReservadasC();
    }
    
    /** ##############################################################################
     * ############################################################################## */
    
    /** Devuelve la cadena lista para ser procesada en el analizadorLR, null en el caso de que haya
     * inconcistencias en el tipo de datos
     */
    public String preAnalisisLexico(String cadenaIngresada){
        String cadenaRdo = "";
        
        // Obtenemos una lista compuesto por los caracteres de la cadena quitando los espacios vacios
        List<String> cadenaSeparada = AnalizadorCadenas.separarCadenaCaracter(cadenaIngresada);
        // Agrupamos los caracteres en token
        List<String> listaToken = crearListaToken(cadenaSeparada);
        System.out.println("Lista token: "+listaToken);
        // Preclasificamos esos tokens para luego ser analizados por el analizador lexico
        List<String> listaTipos = crearListaTiposToken(listaToken);
        if(listaTipos == null){
            System.out.println("Uno de los token ingresados es incorrecto");
            return null;
        }
        System.out.println("Lista tipos token: "+listaTipos);
        
        for (String token : listaTipos) {
            cadenaRdo += token;
        }
        return cadenaRdo;
    }
    
    /* ######################### Metodos Privados #################################
    ############################################################################## */
    
    /* Separa los una lista de caracteres en token delimitados por espacios o caracteres
       especiales como ;, {, ( y otras propias del lenguaje
    */
    private List<String> crearListaToken(List<String> listaCaracteres){
        List<String> listaToken = new ArrayList<>();
        String buffer = "";
        // recorremos la lista analizando separando en token
        for(String caracter : listaCaracteres) {
            if(!this.palabrasReservadasDelLenguaje.esSeparadorVacio(caracter)){
                if(this.palabrasReservadasDelLenguaje.esSeparadorAlmacenable(caracter)){
                    if(!buffer.equals("")){
                        listaToken.add(buffer);
                    }
                    listaToken.add(caracter);
                    buffer = "";
                }
                else{
                    buffer += caracter;
                }
            }
            // si es un separador vacio
            else{
                if(!buffer.equals("")){
                    listaToken.add(buffer);
                    buffer = "";
                }
            }
        }
        if(!buffer.equals("")){
            listaToken.add(buffer);
            buffer = "";
        }
        return listaToken;
    }
    
    /* Devuelve una lista reemplazando los token con su tipo correspondiente
    */
    private List<String> crearListaTiposToken(List<String> listaToken){
        List<String> listaTipoDeDatos = new ArrayList<>();
        String tipoDeDato = "";
        
        for (String token : listaToken) {
            tipoDeDato = tipoDeDatoToken(token);
            if(tipoDeDato != null){
                listaTipoDeDatos.add(tipoDeDato);
            }
            else
                return null;
        }
        return listaTipoDeDatos;
    }
        
    // determina el tipo de dato del token
    private String tipoDeDatoToken(String token){
        // controla si el token pertenece a alguna palabra permitida por el lenguaje
        if(this.palabrasReservadasDelLenguaje.esTipoDeDato(token)){
            return PalabrasReservadasC.TIPO_DE_DATO;
        }
        if(this.palabrasReservadasDelLenguaje.esIncremento(token)){
            return PalabrasReservadasC.INCREMENTO;
        }
        if(this.palabrasReservadasDelLenguaje.esOperadorMatematico1(token)){
            return PalabrasReservadasC.OPERADOR_MATEMATICO_1;
        }
        if(this.palabrasReservadasDelLenguaje.esOperadorMatematico2(token)){
            return PalabrasReservadasC.OPERADOR_MATEMATICO_2;
        }
        if(this.palabrasReservadasDelLenguaje.esComparadorInicio(token)){
            return PalabrasReservadasC.COMPARADOR_INICIO;
        }
        if(this.palabrasReservadasDelLenguaje.esComparadorFin(token)){
            return PalabrasReservadasC.COMPARADOR_FIN;
        }
        if(this.palabrasReservadasDelLenguaje.esComparadorDistinto(token)){
            return PalabrasReservadasC.COMPARADOR_DISTINTO;
        }
        if(token.equals(PalabrasReservadasC.FIN_INSTRUCCION)){
            return PalabrasReservadasC.FIN_INSTRUCCION;
        }
        if(token.equals(PalabrasReservadasC.LOGICO_OR)){
            return PalabrasReservadasC.LOGICO_OR;
        }
        if(token.equals(PalabrasReservadasC.LOGICO_AND)){
            return PalabrasReservadasC.LOGICO_AND;
        }
        if(this.palabrasReservadasDelLenguaje.esSentenciaCiclo(token)){
            return PalabrasReservadasC.SENTENCIA_CICLO;
        }
        if(token.equals(PalabrasReservadasC.PARENTESIS_ABRIR)){
            return PalabrasReservadasC.PARENTESIS_ABRIR;
        }
        if(token.equals(PalabrasReservadasC.PARENTESIS_CERRAR)){
            return PalabrasReservadasC.PARENTESIS_CERRAR;
        }
        if(token.equals(PalabrasReservadasC.LLAVE_ABRIR)){
            return PalabrasReservadasC.LLAVE_ABRIR;
        }
        if(token.equals(PalabrasReservadasC.LLAVE_CERRAR)){
            return PalabrasReservadasC.LLAVE_CERRAR;
        }
        if(this.palabrasReservadasDelLenguaje.esNombreVariable(token)){
            return PalabrasReservadasC.OPERADOR;
        }
        System.out.println(token);
        return null;
    }
    
    // ######################################## Ver si borrar ########################################
    
    // Analiza si una lista de token pertenece al lenguaje
//    private boolean listaTokenValida(List<String> listaToken){
//        for (String token : listaToken) {
//            if(!tokenValido(token)){
//                System.out.println("token invalido: "+token+" Lista: "+listaToken.toString());
//                return false;
//            }
//        }
//        return true;
//    }
//    
//        // determina si un token pertenece al lenguaje
//    private boolean tokenValido(String token){
//        // controla si el token pertenece a alguna palabra permitida por el lenguaje
//        if(this.palabrasReservadasDelLenguaje.esTipoDeDato(token)){
//            return true;
//        }
//        if(this.palabrasReservadasDelLenguaje.esOperadorMatematico1(token)){
//            return true;
//        }
//        if(this.palabrasReservadasDelLenguaje.esOperadorMatematico2(token)){
//            return true;
//        }
//        if(this.palabrasReservadasDelLenguaje.esSentenciaCiclo(token)){
//            return true;
//        }
//        if(this.palabrasReservadasDelLenguaje.esNombreVariable(token)){
//            return true;
//        }
//        return false;
//    }

    
}
