/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dataSystem.PalabrasReservadasC;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import model.AutomataDePila;
import model.DatosSeguimientoPila;
import model.Estado;
import model.Transicion;

/**
 * Esta clase se ocupa de intentar analizar si las cadenas ingresadas son validas para
 * el automata de pila designado.
 * @author emanuel
 */
//@TODO todabia debe verificarse que funciona al 100%
public class AnalizadorLexico {

    private Stack<String> pila;
    private AutomataDePila automata;
    private List<String> listaSeguimientoPila;
    private List<DatosSeguimientoPila> seguimientoPila;

    public AnalizadorLexico(AutomataDePila adp) {
        this.pila = new Stack<String>();
        this.automata = adp;
        this.listaSeguimientoPila = new ArrayList<>();
        this.seguimientoPila = new ArrayList<>();
    }
    /**
     * Deberia intentar analizar
     * @param cadena
     * @return null si la cadena no pertenece al lenguaje, en otro caso podria devolver
     * un estado, este puede ser aceptador, o no.
     */
    public Estado analizarCadena(String cadena) {
        Estado estadoActual = automata.getEstado(0);
        String cadenaSeguimiento = "";
//        Analiza landa para iniciar el automata y guardar el numeral
        estadoActual = analizarCaracter("-", estadoActual);
//        Analiza cada uno de los caracteres de la cadena 
        for (int i = 0; i < cadena.length(); i++) {
            estadoActual = analizarCaracter(String.valueOf(cadena.charAt(i)), estadoActual);
            if(estadoActual == null){
                this.seguimientoPila.add(new DatosSeguimientoPila(String.valueOf(cadena.charAt(i)), "ERROR", false, String.valueOf(pila)));
                return estadoActual;
            }
            cadenaSeguimiento = "analizando " + cadena.charAt(i) + " -> " + estadoActual + "  " + this.pila;
            this.seguimientoPila.add(new DatosSeguimientoPila(String.valueOf(cadena.charAt(i)), estadoActual.getNombre(), estadoActual.isAceptable(), String.valueOf(pila)));
            System.out.println(cadenaSeguimiento);
            this.listaSeguimientoPila.add(cadenaSeguimiento);
        }
        //Analizamos si con FDC($) se llega a un estado aceptador
        estadoActual = analizarCaracter(PalabrasReservadasC.FIN_DE_CADENA, estadoActual);
        if( estadoActual == null){
            cadenaSeguimiento = "analizando " +PalabrasReservadasC.FIN_DE_CADENA+" -> caracter invalido en este estado : ERROR!!! ";
            this.seguimientoPila.add(new DatosSeguimientoPila(PalabrasReservadasC.FIN_DE_CADENA, "ERROR", false, String.valueOf(this.pila)));
        }
        else{
            cadenaSeguimiento = "analizando " +PalabrasReservadasC.FIN_DE_CADENA+" -> " + estadoActual + "  " + this.pila;
            this.seguimientoPila.add(new DatosSeguimientoPila(PalabrasReservadasC.FIN_DE_CADENA, estadoActual.getNombre(), estadoActual.isAceptable(), String.valueOf(pila)));
        }
        System.out.println(cadenaSeguimiento);
        return estadoActual;
    }
    
    private Estado analizarCaracter(String carater, Estado estadoActual) {
//        Busca las transiciones del estado actual y verifica si el caracter ingresado se encuentra en alguna de estas
//        transiciones de ser asi, retorna el estado adondo va la transicion SI no llega a encontrar la transicion con el
//        caracter de entrada, entonces retorna null
        List<Transicion> transiciones = automata.buscarTransisiones(estadoActual.getNombre());
        for (Transicion transicion : transiciones) {
          //  System.out.println("caracter analizado: "+carater+" - Datos transicion: "+transicion.getEntrada()+" "+transicion.getEstadoSucesor()+" "+transicion.getApila()+" "+transicion.getDesapila());
            if (transicion.getEntrada().equals(carater)) {
                if(actualizarPila(transicion) == true){
                    return automata.buscarEstado(transicion.getEstadoSucesor());
                }
                else{
                    return null;
                }
            }
        }
        return null;
    }

    private boolean actualizarPila(Transicion transicion) {
//      si lo que tenemos que desapilar es distinto de landa, entonces se verifica que lo que este en el tope de la pila sea lo que
//      se debe desapilar, es decir, (si el elemento que esta en el tope de la pila es igual al valor que la transicion te pide
//      desapilar entonces lo desapila), si esto no ocurre el automata se rompe devido a que no encontraste lo que necesitas desapilar.

        if (!transicion.getDesapila().equals("-")) {
            if(!pila.empty()){    
                if (pila.peek().equals(transicion.getDesapila())) {
                        pila.pop();
                    }else{
                        return false;
                    }
            }
            else
            {
                return false;
            }
        }
//      Al momento de apila, es mas simple porque solo hay que apilar lo que indique la transicion...
        if (!transicion.getApila().equals("-")) {
            pila.push(transicion.getApila());
        }
        
        return true;
    }
    public void destruirPila(){
        this.pila.clear();
    }
    
    // devuelve el seguimiento de la pila
    public String imprimirSeguimientoPila(){
        String seguimiento = "";
        for (String lineaSeguimiento : listaSeguimientoPila) {
            seguimiento += lineaSeguimiento + "\n";
        }
        return seguimiento;
    }
    
    // devuelve una matriz con los datos de seguimiento de la pila
    public String[][] getDatosSeguimiento(){
        // creamos la matriz con los datos de seguimiento
        String[][] datosSeguimiento = new String[this.seguimientoPila.size()][4];
        // vamos completando los datos
        int nroFila = 0;
        for (DatosSeguimientoPila datos : this.seguimientoPila) {
            // completamos los datos de las columnas
            datosSeguimiento[nroFila][0] = datos.getCaracter();
            datosSeguimiento[nroFila][1] = datos.getEstadoDestino();
            datosSeguimiento[nroFila][3] = datos.getContenidoPila();
            if(datos.isAceptador()){
                datosSeguimiento[nroFila][2] = "SI";
            }
            else{
                datosSeguimiento[nroFila][2] = "NO";
            }
            nroFila++;
        }
        return datosSeguimiento;
    }
    
    public void resetearDatos(){
        this.listaSeguimientoPila.clear();
        this.seguimientoPila.clear();
        this.pila.clear();
        System.out.println("***************** ANALIZADOR LEXICO *****************");
        System.out.println("Estado de la pila: "+this.pila);
        System.out.println("Estado de la lista de seguimiento: "+this.listaSeguimientoPila.toString());
        System.out.println("Estado de la lista de seguimiento de datos: "+this.seguimientoPila.toString());
    }
    
    // vacia la lista de seguimiento
    public void limpiarListaSeguimiento(){
        this.listaSeguimientoPila.clear();
    }
    
    public void limpiarDatosSeguimiento(){
        this.seguimientoPila.clear();
    }

    public AutomataDePila getAutomata() {
        return automata;
    }

    public List<String> getDatosPila() {
        List<String> datosPila = new ArrayList<>();
        List<String> auxDatosPila = new ArrayList<>();
        while (!this.pila.isEmpty()) {            
            auxDatosPila.add(this.pila.pop());
        }
        for (int i = (auxDatosPila.size() - 2); i >= 0 ; i--) {
            datosPila.add(auxDatosPila.get(i));
        }
        datosPila.add(PalabrasReservadasC.FIN_DE_CADENA);
        return datosPila;
    }
    
    public List<DatosSeguimientoPila> getSeguimientoPila() {
        return seguimientoPila;
    }

}
