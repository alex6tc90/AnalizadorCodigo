/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author alextc6
 */
public class ControladorTablaLR {
    
    private Object[][] tablaLR;
    
    public ControladorTablaLR(Object[][] tabla){
        this.tablaLR = tabla;
    }
    
    /**
     * Busca el indice del simbolo dentro de la tabla
     * @param simbolo elemento buscado
     * @return indice correspondiente al simbolo buscado, -1 en caso de no encontrarlo
     */
    public int getIndiceSimbolo(String simbolo){
            for (int i = 1; i < (this.tablaLR[0]).length; i++) {
                if( simbolo.equals((String)this.tablaLR[0][i])){
                    return i;
                }
            }
            return -1;
    }
    
    public String mostrarTabla() {
        String string = "";
            for (int i = 0; i < this.tablaLR.length; i++) {
                if (i == 1) {
                    string += "\n---------------------------------";
                }
                string += "\n";
                for (int j = 0; j < this.tablaLR[i].length; j++) {
                    if(this.tablaLR[i][j] != null){
                        string += this.tablaLR[i][j] + "   ";
                    }
                    else{
                        string += " -   ";
                    }
                }
            }
            return string;
    }
    
    public Object getDatoTabla(int fila, int col){
        return this.tablaLR[fila][col];
    }
    
    public Object[][] getTablaLR() {
        return tablaLR;
    }
    
    public List<String> getSimbolos(){
        List<String> simbolos = new ArrayList<>();
        // recuperemos los simbolos de la tabla
        for (int i = 1; i < (this.tablaLR[0].length); i++) {
            simbolos.add((String)(this.tablaLR[0][i]));
        }
        return simbolos;
    }
    
}
