/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import dataSystem.PalabrasReservadasC;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import model.Aceptador;
import model.Desplazamiento;
import model.Reduccion;
import services.AnalizadorCadenas;

/**
 *
 * @author alextc6
 */
public class AnalizadorSintactico {
    
    private Stack<String> pilaDatos;
    private Stack<String> colaSimbolos;
    private int estadoActual;
    private ControladorTablaLR controladorTabla;
    private List<String> rdoAnalisis;
    
    private final static int ESTADO_INICIAL = 1;
    
    public AnalizadorSintactico(ControladorTablaLR contTabla){
        this.controladorTabla = contTabla;
        this.pilaDatos = new Stack<String>();
        this.colaSimbolos = new Stack<String>();
        this.estadoActual = ESTADO_INICIAL;
        this.rdoAnalisis = new ArrayList<>();
    }
    
    public boolean validarCadena(List<String> listaSimbolos){
        System.out.println("Simbolos: "+this.controladorTabla.getSimbolos().toString());
        llenarColaSimbolos(listaSimbolos);
        System.out.println("Simbolos cola: "+this.colaSimbolos);
        // apilamos el estado inicial en la pila
        this.pilaDatos.push(String.valueOf(this.estadoActual));
        int indiceSimbolo;
        String simboloLeido = this.colaSimbolos.pop();
        String izqRegla;
        String derRegla;
        Object auxValorTabla;
        // obtenemos el valor inicilal de la tabla
        Object valorTabla = this.controladorTabla.getDatoTabla(this.estadoActual, this.controladorTabla.getIndiceSimbolo(simboloLeido));
        System.out.println("Simbolo leido: "+simboloLeido+" ----> insert1ro: "+simboloLeido+" - insert2do: "+String.valueOf(this.estadoActual));
        // leemos cada uno de los simbolos de la cadena
        while(!(valorTabla instanceof Aceptador)) {
            // analizamos el tipo de accion correspondiente al valor de la tabla
            if(valorTabla instanceof Desplazamiento){
                this.pilaDatos.push(simboloLeido);
                this.estadoActual = ((Desplazamiento)valorTabla).getNuevoEstado();
                this.pilaDatos.push(String.valueOf(this.estadoActual));
                System.out.println(simboloLeido+": DESP =====> "+((Desplazamiento)valorTabla).toString()+" simbLeido: "+simboloLeido+" - estActual:"+estadoActual+" insPila:("+simboloLeido+","+estadoActual+") ==== PILA: "+pilaDatos);
                simboloLeido = this.colaSimbolos.pop();
            }
            else{
                if(valorTabla instanceof Reduccion){
                    System.out.println("Entro a reduccion");
                    // extraer lado derecho de la regla de la pila
                    derRegla = ((Reduccion)valorTabla).getDerRegla();
                    // extraer derRegla de la pila y guardar nro que esta arriba de el
                    extraerDatoPila(derRegla);
                    this.estadoActual = Integer.valueOf(this.pilaDatos.peek());
                    // guardams el lado izq de la regla en la pila
                    izqRegla = ((Reduccion)valorTabla).getIzqRegla();
                    this.pilaDatos.push(izqRegla);
                    indiceSimbolo = this.controladorTabla.getIndiceSimbolo(izqRegla);
                    if(indiceSimbolo == -1){
                        System.out.println(" El simbolo "+izqRegla+" NO ES CORRECTO");
                        return false;
                    }
                    auxValorTabla = this.controladorTabla.getDatoTabla(this.estadoActual, indiceSimbolo);
                    if(auxValorTabla == null){
                        System.out.println(" Subindices: ["+this.estadoActual+"]["+izqRegla+"] ---- : indSimbolo: "+indiceSimbolo);
                        return false;
                    }
                    this.estadoActual = ((Desplazamiento)auxValorTabla).getNuevoEstado();
                    this.pilaDatos.push(String.valueOf(this.estadoActual));
                    System.out.println(simboloLeido+": RED =====> "+((Reduccion)valorTabla).toString()+" simbLeido: "+simboloLeido+" - estActual:"+estadoActual+" insPila:("+simboloLeido+","+estadoActual+") ==== PILA: "+pilaDatos);
                }
                else{
                    if(valorTabla == null){
                        // error
                        System.out.println("Ha ocurrido un error en los datos de la tabla: NULL");
                        return false;
                    }
                }
            }
            // ********** reemplazar 2do subindice con metodo que obtiene el indice del simbolo(contTabla)
            System.out.println("indices "+this.estadoActual+" - "+this.controladorTabla.getIndiceSimbolo(simboloLeido));
            indiceSimbolo = this.controladorTabla.getIndiceSimbolo(simboloLeido);
            if(indiceSimbolo == -1){
                System.out.println(" El simbolo "+simboloLeido+" NO ES CORRECTO");
                return false;
            }
            valorTabla = this.controladorTabla.getDatoTabla(this.estadoActual, indiceSimbolo);
            if(valorTabla == null){
                System.out.println("ValorTabla = NULL!!!!!!!!!  -- "+this.estadoActual+" , " +this.controladorTabla.getIndiceSimbolo(simboloLeido));
                return false;
            }
            System.out.println(" valor de la tabla["+this.estadoActual+"]"+"["+this.controladorTabla.getIndiceSimbolo(simboloLeido)+"]: "+valorTabla.toString());
        }
        // vemos si es fin de cadena
        if((!simboloLeido.equals(PalabrasReservadasC.FIN_DE_CADENA))||(!(valorTabla instanceof Aceptador))){
            // error
            return false;
        }
        vaciarPila();
        return true;
    }
    
    public void reiniciarDatos(){
        this.pilaDatos = new Stack<String>();
        this.colaSimbolos = new Stack<String>();
        System.out.println("***************** ANALIZADOR SINTACTICO *****************");
        System.out.println("Estado de la pila: "+this.pilaDatos);
        System.out.println("Estado de la cola de simbolos: "+this.colaSimbolos);
        this.estadoActual = ESTADO_INICIAL;
    }

    public List<String> getCadenaResultado() {
        return rdoAnalisis;
    }
    
    private void llenarColaSimbolos(List<String> simbolos){
        if(simbolos.isEmpty())
            System.out.println("La lista ingresada es vacia");
        for (int i = simbolos.size() -1; i >= 0; i--) {
            this.colaSimbolos.push(simbolos.get(i));
        }
    }
    
    // saca los datos correspondientes de la pila
    private void extraerDatoPila(String dato){
        String cimaPila = this.pilaDatos.peek();
        List<String> sacadosPila = new ArrayList<>();
        // separarams el string en varios string por cada caracter
        List<String> cadenaSeparada = AnalizadorCadenas.separarCadenaCaracter(dato);
        // recuperamos el primer dato de la cadena
        String datoCorte = cadenaSeparada.get(0);
        // buscamos la cant de repeticiones del dato
        int repeticionesDatoCorte = 0;
        for (String cadena : cadenaSeparada) {
            if(cadena.equals(datoCorte)){
                repeticionesDatoCorte++;
            }
        }
        
        System.out.println("Dato buscado: "+datoCorte+" --- cima de pila: "+cimaPila+" --- pila: "+this.pilaDatos);
        
        // sacamos de la pila hasta encontrar el dato
        int i = 0;
        while (i < repeticionesDatoCorte) {
            cimaPila = this.pilaDatos.peek();
            if(cimaPila.equals(datoCorte)){
                i++;
            }
            System.out.println("Estado de pila antes del pop: "+this.pilaDatos+" --- repeticiones: "+repeticionesDatoCorte);
            sacadosPila.add(cimaPila);
            this.pilaDatos.pop();
        }
        
        System.out.println("Termino de extraer datos de la pila ---- datos sacados: "+sacadosPila.toString());
        System.out.println(" Datos de pila: "+this.pilaDatos);
    }
    
    private void vaciarPila(){
        while(!(this.pilaDatos.isEmpty())){
            this.rdoAnalisis.add(this.pilaDatos.peek());
            this.pilaDatos.pop();
        }
    }
    
}
