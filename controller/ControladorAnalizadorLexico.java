package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import model.AutomataDePila;
import model.Estado;
import model.Transicion;

/**
 * Clase encargada de parsear la entrada estandar en un automata
 *
 * @author Ema Tu padre
 */
public class ControladorAnalizadorLexico {

    private AnalizadorLexico analizadorLexico;
    private BufferedReader bufferDatos;
    private List<String> tokensRdoAnalisisLexico;
    
    /**
     * Constructor de la clase
     */
    public ControladorAnalizadorLexico(BufferedReader bufferDatos) throws IOException {
        this.bufferDatos = bufferDatos;
        this.analizadorLexico = new AnalizadorLexico(this.generateAutomata());
        this.tokensRdoAnalisisLexico = new ArrayList<>();
    }
    
    // determina si la cadena ingresada es aceptada o no por el automata
    public boolean cadenaValida(String cadena){
        Estado estadoFinal = this.analizadorLexico.analizarCadena(cadena);
        if(estadoFinal == null){
            return false;
        }
        if(!estadoFinal.isAceptable()){
            return false;
        }
        // recuperamos los datos del analisis lexico
        this.tokensRdoAnalisisLexico = this.analizadorLexico.getDatosPila();
        return true;
    }

    public List<String> getTokensRdoAnalisisLexico() {
        return tokensRdoAnalisisLexico;
    }
    
    public void resetDatosAnalizador(){
        this.analizadorLexico.resetearDatos();
    }

    /**
     * Este metodo genera un automata a partir de un archivo
     * @param in buffer de entrada
     * @return el automata generado
     * @throws IOException
     */
//    @TODO refactorizar todabia lo hace a lo bestia
    private AutomataDePila generateAutomata() throws IOException {
        
        String line;
        // leemos y guardamos la linea del lenguaje desde el txt(1ra linea)
        line = this.bufferDatos.readLine();
        
        // inicializacion de variables
        AutomataDePila automata = new AutomataDePila(line);
        
        while (this.bufferDatos.ready()) {
            line = this.bufferDatos.readLine();
            // Mientras haiga lineas por leer
            if (line != null) {
                // Guarda los valores separados por espacio en un vector
                String[] values = line.split("\\s+");
                if (values.length == 3) { 
                    // si la info pertenece a un estado entonces lo crea y lo agrega al automata
                    Estado estado = new Estado(values[0], (values[1].equals("1")) ? true : false);
                    automata.agregarEstado(estado);
                    //  Crea la lista de transiciones 
                    int cantidadTransiciones = Integer.decode(values[2]);
                    List<Transicion> transiciones = new ArrayList<>();
                    //  Carga las transiciones 
                    for (int i = 0; i < cantidadTransiciones; i++) {
                        line = this.bufferDatos.readLine();
                    // separa la info de la transicion por espacios
                        String[] transicion = line.split("\\s+");
                        transiciones.add(new Transicion(transicion[0],transicion[1], transicion[2], transicion[3]));
                    }
                    // Agrega las transiciones al automata
                    automata.agregarTransiciones(estado.getNombre(), transiciones);
                }
            }
        }
        System.out.println("####### Se cargo el txt del automata #########");
        System.out.println("******** Automata cargado *********");
        System.out.println(automata);
        return automata;
    }
}
