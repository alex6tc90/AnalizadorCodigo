/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import servicios.AdministradorArchivos;

/**
 *
 * @author alextc6
 */
public class ControladorSistema {
    
    private ControladorAnalizadorSintactico contAnalizadorSintactico;
    private ControladorAnalizadorLexico contAnalizadorLexico;
    private TokenizadorC tokenizador;
    private AdministradorArchivos adminArchivos;
    
    public ControladorSistema(String datosAnalizadorLexico, String datosAnalizadorSintactico, TokenizadorC tokenizador) throws IOException{
        this.adminArchivos = AdministradorArchivos.getAdminArchivos();
        this.contAnalizadorLexico = new ControladorAnalizadorLexico(this.adminArchivos.getBufferDatos(datosAnalizadorLexico));
        this.contAnalizadorSintactico = new ControladorAnalizadorSintactico(this.adminArchivos.getBufferDatos(datosAnalizadorSintactico));
        this.tokenizador = tokenizador;
        
    }
    
    public boolean analizarCadena(String cadenaIngresada){
        // prmier analisis de la cadena
        System.out.println("******************** Tokenizador ********************");
        System.out.println("Cadena ingresada: "+cadenaIngresada);
        String cadenaPreAnalizada = this.tokenizador.preAnalisisLexico(cadenaIngresada);
        if(cadenaPreAnalizada == null){
            System.out.println("Hubo errores en la etapa de preanalisis de la cadena ingresada.");
            return false;
        }
        System.out.println(" Rdo de tokenizador: "+cadenaPreAnalizada);
        
        // analizador lexico
        System.out.println("******************** Analizador Lexico ********************");
        boolean cadenaValidaAnalisisLexico = this.contAnalizadorLexico.cadenaValida(cadenaPreAnalizada);
        System.out.println("Cadena valida: "+cadenaValidaAnalisisLexico);
        if(!cadenaValidaAnalisisLexico){
            System.out.println("La cadena no pasó el analisis lexico");
            return false;
        }
        List<String> listaTiposToken = this.contAnalizadorLexico.getTokensRdoAnalisisLexico();
        System.out.println("Datos de la pila: "+listaTiposToken);
        
        // analisis sintactico
        System.out.println("******************** Analizador Sintactico ********************");
        boolean rdoAnalisis = this.contAnalizadorSintactico.analizarCadena(listaTiposToken);
        System.out.println("Lista de token: "+rdoAnalisis);
        return rdoAnalisis;
    }
    
    public List<String> getTokenAnlisisLexico(){
        return this.contAnalizadorLexico.getTokensRdoAnalisisLexico();
    }
    
    public List<String> getRdoAnalisisSintactico(){
        return this.contAnalizadorSintactico.getRdoAnalisis();
    }
    
    public void resetearDatos(){
        System.out.println("*************** ¡¡¡¡¡ Datos de los analizadores reseteados !!!!! *************************");
        System.out.println("******************************************************************************************");
        this.contAnalizadorSintactico.resetDatosAnalizador();
        this.contAnalizadorLexico.resetDatosAnalizador();
    }
    
}