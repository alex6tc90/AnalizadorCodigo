package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;
import model.Aceptador;

//import model.Automata;
import model.Desplazamiento;
//import model.Member;
import model.Reduccion;
//import model.Status;

/**
 * Clase encargada de parsear la entrada estandar en un automata
 * 
 * @author Ema Tu padre
 */
public class ControladorAnalizadorSintactico {

	private Object[][] matrizPrincipal;
        private final static String DESPLAZAMIENTO = "D";
        private final static String REDUCCION = "R";
        private final static String DESPLAZAMIENTO_Y_REDUCCION = "DR";
        private final static String ACEPTACION = "A";
        private final static String FIN_DE_CADENA = "$";
        private ControladorTablaLR contTablaLR;
        private AnalizadorSintactico analizadorSintatico;
        private BufferedReader bufferDatos;
        
    	/**
	 * Constructor de la clase
	 */
	public ControladorAnalizadorSintactico(BufferedReader bufferDatos) throws IOException {
            this.bufferDatos = bufferDatos;
            this.contTablaLR = new ControladorTablaLR(cargarMatriz(this.bufferDatos));
            this.analizadorSintatico = new AnalizadorSintactico(this.contTablaLR);
	}
        
        public boolean analizarCadena(List<String> listaToken){
            return this.analizadorSintatico.validarCadena(listaToken);
        }
        
        public List<String> getRdoAnalisis(){
            return this.analizadorSintatico.getCadenaResultado();
        }
        
        public void resetDatosAnalizador(){
            this.analizadorSintatico.reiniciarDatos();
        }
        
        private Object[][] cargarMatriz(BufferedReader datosTxt) throws IOException { 
            // obtenemos la dimension de la matriz por medio del txt
            String nroEstados = datosTxt.readLine();
            String nroSimbolos = datosTxt.readLine();
            // creamos la matriz del tamaño especificado
            this.matrizPrincipal = new Object[Integer.parseInt(nroEstados) + 1] [Integer.parseInt(nroSimbolos) + 1];
            System.out.println("Matriz de "+nroEstados+" x "+nroSimbolos);
            // cargamos los simbolos y estados
            cargarSimbolos(datosTxt.readLine());
            cargarEstados(Integer.valueOf(nroEstados));
            // cargamos las acciones de la tabla
            cargarDatos(datosTxt);
            System.out.println("****** Se cargaron los datos de la tabla en su controlador ******");
            System.out.println(this.imprimirMatriz());
            return this.matrizPrincipal;
        }
        
        private void cargarDatos(BufferedReader datosEntrada) throws IOException{
            int nroEstadoActual;
            String tipoAccion;
            int cantDatos;
            int cantDatosAux;
            String lineaLeida;
            
            while (datosEntrada.ready()) {
                lineaLeida = datosEntrada.readLine();
                System.out.println("******** Linea leida: "+lineaLeida+" ********");
                if (lineaLeida != null) {
                    // obtenemos el nro de estado, tipo de accion y la cant de datos a cargar
                    String[] datosLinea = lineaLeida.split("\\s+"); 
                    nroEstadoActual = Integer.parseInt((datosLinea[0]).toString());
                    tipoAccion = datosLinea[1];
                    cantDatos = Integer.valueOf(datosLinea[2]);
                    
                    // vemos que tipo de accion hay que realizar
                    if(tipoAccion.equals(this.DESPLAZAMIENTO)){
                        // vamso a crear y guardar los desplazamientos correpondientes
                        cargarDesplazamientos(nroEstadoActual, datosEntrada, cantDatos);
                    }
                    if(tipoAccion.equals(this.REDUCCION)){
                        // vamso a crear y guardar la cant de reducciones correpondientes
                        cargarReducciones(nroEstadoActual, datosEntrada, cantDatos);
                    }
                    if(tipoAccion.equals(this.DESPLAZAMIENTO_Y_REDUCCION)){
                        // obtenemos la cant de reducciones, ya que en este caso el 1er valor es la cant de desplazamientos
                        cantDatosAux = Integer.valueOf(datosLinea[3]);
                        // vamso a crear y guardar los desplazamientos correpondientes
                        cargarDesplazamientos(nroEstadoActual, datosEntrada, cantDatos);
                        // vamso a crear y guardar la cant de reducciones correpondientes
                        cargarReducciones(nroEstadoActual, datosEntrada, cantDatosAux);
                    }
                    if(tipoAccion.equals(this.ACEPTACION)){
                        // vamso a crear y guardar el estado de aceptacion
                        cargarAceptacion(nroEstadoActual);
                    }
                }
            }
        }
        
        private void cargarSimbolos(String lineaSimbolos){
            // separamos por espacios dejando cada componente en un arreglo
            String[] simbolos = lineaSimbolos.split("\\s+");
            // agregamos ls simbolos a la matriz
            for (int i = 0, j = 1; j <= simbolos.length; i++, j++) {
		this.matrizPrincipal[0][j] = simbolos[i];
            }
        }
        
        private void cargarEstados(int cantEstados){
            for (int i = 1; i <= cantEstados; i++) {
                this.matrizPrincipal[i][0] = i;
            }
        }
        
        private void cargarDesplazamientos(int estadoActual, BufferedReader bufferLectura, int cantDatos) throws IOException{
            String simbolo;
            int estadoNuevo;
            
            for (int i = 0; i < cantDatos; i++) {
                String lineaLeida = bufferLectura.readLine();
                String[] datosLinea = lineaLeida.split("\\s+");
                // recuperamos los datos para crear el desplazamiento
                simbolo = datosLinea[0];
                estadoNuevo = Integer.parseInt(datosLinea[1]);
                // asignamos el valor en su lugar correspondiente
                this.matrizPrincipal[estadoActual][getIndiceSimbolo(simbolo)] = new Desplazamiento(estadoNuevo);
            }
            
        }
        
        private void cargarReducciones(int estadoActual, BufferedReader bufferLectura, int cantDatos) throws IOException{
            String simbolo;
            String valorIzqRegla;
            String valorDerRegla;
            
            for (int i = 0; i < cantDatos; i++) {
                String lineaLeida = bufferLectura.readLine();
                String[] datosLinea = lineaLeida.split("\\s+");
                // recuperamos los datos para crear la reduccion
                simbolo = datosLinea[0];
                valorIzqRegla = datosLinea[1];
                valorDerRegla = datosLinea[2];
                this.matrizPrincipal[estadoActual][getIndiceSimbolo(simbolo)] = new Reduccion(valorIzqRegla, valorDerRegla);
            }
        }
        
        private void cargarAceptacion(int estadoActual){
            this.matrizPrincipal[estadoActual][getIndiceSimbolo(this.FIN_DE_CADENA)] = new Aceptador();
        }
        
        private int getIndiceSimbolo(String simbolo){
            String simComp = "";
            for (int i = 1; i < (this.matrizPrincipal[0]).length; i++) {
                if( simbolo.equals((String)this.matrizPrincipal[0][i])){
                    simComp = ((String)this.matrizPrincipal[0][i]);
                    return i;
                }
            }
            System.out.println("simbolo error: "+simbolo+" --- simb comp: "+simComp);
            return -1;
        }
        
        // ################################# Ver si borrar #################################
        public String imprimirMatriz() {
        String string = "";
            for (int i = 0; i < this.matrizPrincipal.length; i++) {
                if (i == 1) {
                    string += "\n---------------------------------";
                }
                string += "\n";
                for (int j = 0; j < this.matrizPrincipal[i].length; j++) {
                    string += this.matrizPrincipal[i][j] + "\t";
                }
            }
            return string;
        }    

}